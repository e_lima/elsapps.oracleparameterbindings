﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using System.Data;

namespace elsapps.OracleParameterBindings
{
    public static class Extensions
    {
        public static OracleParameter AddWithBinding(this OracleParameterCollection thiz, string name, object value, string bindingType, ParameterDirection direction = ParameterDirection.Input)
        {
            var param = new OracleParameter();
            param.OracleDbType = Oracle.DataAccess.Client.OracleDbType.Array;
            param.Value = value;
            param.Direction = direction;
            param.ParameterName = name;
            param.UdtTypeName = bindingType;

            thiz.Add(param);
            return param;
        }
    }
}
