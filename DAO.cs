﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace elsapps.OracleParameterBindings
{
    internal class DAO
    {
        OracleConnection _conn;

        public DAO(OracleConnection c)
        {
            _conn = c;
        }

        public bool TypeExists(string type)
        {
            using (var cmd = _conn.CreateCommand())
            {
                const string CMD_TEXT = @"SELECT 1 FROM all_types WHERE TYPE_NAME = :type_name";
                cmd.CommandText = CMD_TEXT;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.Add("type_name", type);

                var scalar = cmd.ExecuteScalar();

                return scalar != null;
            }
        }

        public void DefineType(string type, Type typeInfo)
        {
            var i = typeInfo.GetInterfaces();
            var t = Activator.CreateInstance(typeInfo);

            if (i.Contains(typeof(IOracleArrayTypeFactory))) //Array type -- Creates VARRAY binding
            {
                using (var cmd = _conn.CreateCommand())
                {
                    const string CMD_TEXT = @"CREATE OR REPLACE TYPE {0} AS VARRAY({1}) OF {2}";

                    cmd.CommandText = String.Format(CMD_TEXT, type, Constants.DEFAULT_ARRAY_SIZE, (t as IFactoryImpl).GetDbTypeString());
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Parameters.Add("type_name", type);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void EnsureTypeDefinition(string type, Type typeInfo)
        {
            if (!TypeExists(type))
            {
                DefineType(type, typeInfo);
            }
        }
    }
}
