﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Types;

namespace elsapps.OracleParameterBindings
{
    
    [OracleCustomTypeMappingAttribute(Constants.TYPE_INT_ARRAY)]
    public class IntArrayFactory : IOracleArrayTypeFactory, IFactoryImpl
    {
        public Array CreateArray(int numElems)
        {
            return new int[numElems];
        }

        public Array CreateStatusArray(int numElems)
        {
            throw new NotImplementedException();
        }

        public string GetDbTypeString()
        {
            return "NUMBER";
        }
    }

    [OracleCustomTypeMappingAttribute(Constants.TYPE_STRING_ARRAY)]
    public class StringArrayFactory : IOracleArrayTypeFactory, IFactoryImpl
    {
        public Array CreateArray(int numElems)
        {
            return new string[numElems];
        }

        public Array CreateStatusArray(int numElems)
        {
            throw new NotImplementedException();
        }

        public string GetDbTypeString()
        {
            return String.Format("VARCHAR2({0})", Constants.DEFAULT_VARCHAR2_SIZE);
        }
    }
}
