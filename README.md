# ELSAPPS - OracleParameterBindings for .NET

OracleParameterBindings is a library to make easier to pass arrays (for now) to oracle procedures/functions when using Oracle.DataAccess client for .NET.

---


# Installing

Just add a reference to elsapps.OracleParameterBindings.dll in your project.

---


# Usage

## Init(OracleConnection c) method

When using the component against a new/non-initialized schema, the convenience method `Config.Init(...)` can be used to create the supported binding types in the database.  
This method will try to create all the supported types in the target database. *Be alert to database permissions*

---

### int[] binding: EA_OPB_INT_ARRAY

Type mapped from `int[]`  

#### Sample

A procedure with the following signature:  

```
!#sql

create or replace procedure SELECT_PARAMS(p_params in EA_OPB_INT_ARRAY) is
begin

  for item in p_params.first .. p_params.last loop
  begin
    
    -- do something with "item"
    
  end;

  end loop;
end;
```

So, the code call will be like:  

```
//Assuming that conn is an open connection
var cmd = conn.CreateCommand(); -- IDbCommand

cmd.CommandText = "SELECT_PARAMS";
cmd.CommandType = System.Data.CommandType.StoredProcedure;

//Create the parameter
var p_list = cmd.CreateParameter();
p_list.OracleDbType = Oracle.DataAccess.Client.OracleDbType.Array; -- Required
p_list.Value = new [] { 1, 2, 3}; -- Any array
p_list.ParameterName = "p_params";
p_list.UdtTypeName = elsapps.OracleParameterBindings.Constants.TYPE_INT_ARRAY;

cmd.Parameters.Add(p_list);

var return = cmd.ExecuteNonQuery();
...
//Close the connection, etc ... 

```

Alternatively, the parameter may be created using the `AddWithBindings(...)` extension method (requires the namespace elsapps.OracleParameterBindings to be imported)

#### Sample
  
```
using elsapps.OracleParameterBindings;
...  
  
cmd.Parameters.AddWithBinding("p_params", new [] { 1, 2, 3}, elsapps.OracleParameterBindings.Constants.TYPE_INT_ARRAY);
```

---

### string[] binding: EA_OPB_STRING_ARRAY

Type mapped from `string[]`

#### Sample
```
cmd.Parameters.AddWithBinding("p_params", new [] { 1, 2, 3}, elsapps.OracleParameterBindings.Constants.TYPE_STRING_ARRAY);
```

---
# Support / Compatibility

The library was written on top of 4.112.3.0 version of Oracle.DataAcces client.  
Currently, only int[] and string[] are supported mapping to varray of number and varray of varchar2(Constants.DEFAULT_VARCHAR2_SIZE), respectively.


