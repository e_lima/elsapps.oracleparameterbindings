﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Types;

namespace elsapps.OracleParameterBindings
{
    public static class Constants
    {
        public const string TYPE_INT_ARRAY = "EA_OPB_INT_ARRAY";
        public const string TYPE_STRING_ARRAY = "EA_OPB_STRING_ARRAY";

        public static Type ATTRIBUTE_TYPE = typeof(OracleCustomTypeMappingAttribute);
        public static int DEFAULT_ARRAY_SIZE = 2056;
        public static int DEFAULT_VARCHAR2_SIZE = 256;
    }
}
