﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elsapps.OracleParameterBindings
{
    internal interface IFactoryImpl
    {
        string GetDbTypeString();
    }
}
