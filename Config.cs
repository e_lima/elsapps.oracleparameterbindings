﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace elsapps.OracleParameterBindings
{
    public static class Config
    {
        /// <summary>
        /// Call it once
        /// </summary>
        /// <param name="connectionString"></param>
        public static void Init(string connectionString)
        {
            Init(new OracleConnection(connectionString));
        }

        /// <summary>
        /// Call it once
        /// </summary>
        /// <param name="connection"></param>
        public static void Init(OracleConnection connection)
        {
            //Check for factories
            try
            {
                var assy = System.Reflection.Assembly.GetExecutingAssembly();
                var myTypes = assy.GetTypes();

                var dao = new DAO(connection);

                if (connection.State != System.Data.ConnectionState.Open)
                {
                    connection.Open();
                }

                foreach (var item in myTypes)
                {
                    if (item.IsDefined(Constants.ATTRIBUTE_TYPE, false))
                    {
                        var attrs = item.GetCustomAttributes(Constants.ATTRIBUTE_TYPE, false);
                        var attr = attrs[0] as OracleCustomTypeMappingAttribute;
                        dao.EnsureTypeDefinition(attr.UdtTypeName, item);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally 
            {
                connection.Close();
            }
        }
    }
}
